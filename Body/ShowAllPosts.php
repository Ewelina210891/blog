<?php

$query = "SELECT COUNT(*) as all_posts FROM posts";
$result = $db->query($query);
$row = $result->fetch_assoc();
extract($row);

$newsOnPage = 15;
$navNum = 5; 
$allPages = ceil($all_posts/$newsOnPage);
 
if(!isset($_GET['page']) or $_GET['page'] > $allPages or !is_numeric($_GET['page']) or $_GET['page'] <= 0)
{
	$page = 1;
}else
{
    $page = $_GET['page'];
}
$numOfFirstNewsOnPage = ($page - 1) * $newsOnPage;
                
$query = 'SELECT * FROM posts ORDER BY Date DESC LIMIT '.$numOfFirstNewsOnPage.', '.$newsOnPage;
$r = $db->query($query);
 
while($a = $r->fetch_assoc())
{
	
?>
<div id="post_<?php echo $a["PostId"]?>" class="post">

	<h1><a href="Index.php?postId=<?php echo $a["PostId"] ?>">	<?php echo $a["Title"]?>
	</a></h1>

	<?php
	$sql2 = "Select Name from users where UserId=".$a["AuthorId"];
	$authorName = $db->query($sql2);
	$author = $authorName->fetch_assoc()

	?>
	<span class="date"> <?php echo $a["Date"]?>, autor: <?php echo $author["Name"]; ?> </span>

	<div id="postContent" class="postContent">
		<?php echo $a["Content"];

		$sql3 = "Select Count(*) as count from comments where PostId=".$a["PostId"]." ";
		$countOfComments = $db->query($sql3);
		$count = $countOfComments->fetch_assoc()?>
	</div>
	<a href="Index.php?postId=<?php echo $a["PostId"]?>#comments"><span>Komentarze(<?php echo  $count["count"] ?>)
	</span></a>
</div>
<?php
}
if($navNum > $allPages){
$navNum = $allPages;
}

$forstart = $page - floor($navNum/2);
$forend = $forstart + $navNum;

if($forstart <= 0){ $forstart = 1; }

$overend = $allPages - $forend;

if($overend < 0){ $forstart = $forstart + $overend + 1; }

$forend = $forstart + $navNum;
$prev = $page - 1;
$next = $page + 1;

$script_name = $_SERVER['SCRIPT_NAME'];


echo '<div id="nav"><ul>';
if($page > 1) echo '<li><a href="'.$script_name.'?page='.$prev.'">Poprzednia</a></li>';
if ($forstart > 1) echo '<li><a href="'.$script_name.'?page=1">[1]</a></li>';
if ($forstart > 2) echo '<li>...</li>';

for($forstart; $forstart < $forend; $forstart++)
{
	if($forstart == $page)
	{
		echo '<li class="current">';
	}
	else
	{
	echo '<li>';
	}
	echo '<a href="'.$script_name.'?page='.$forstart.'">['.$forstart.']</a></li>';
}

if($forstart < $allPages) echo '<li>...</li>';
if($forstart - 1 < $allPages) echo '<li><a href="'.$script_name.'?page='.$allPages.'">['.$allPages.']</a></li>';
if($page < $allPages) echo '<li><a href="'.$script_name.'?page='.$next.'">Następna</a></li>';
echo '</ul></div><div class="clear"></div>';
?>

