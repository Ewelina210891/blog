<div id="posts" class="col-md-9 posts">
	<?php 
		if(isset($_GET['login']))
		{
			include ('Body/Login.php');			
		}
		else if(isset($_GET['logout']))
		{
			include ('Body/Logout.php');			
		}
		else if(isset($_GET['postId']))
		{
			include ('Body/ShowOnePost.php');	
		}
		else if(isset($_GET['settings']))
		{
			include ('Body/Settings.php');	
		}
		else if(isset($_GET['addPost']))
		{
			include ('Body/AddPost.php');	
		}
		else
		{
			include ('Body/ShowAllPosts.php');
		}			
		?>
</div>