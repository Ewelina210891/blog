<div id="menu" class="col-md-3 menu">
	
	<div id="home" class="menuItem">
		<a href="Index.php">Strona główna</a>
	</div>
	
	<?php 
	if(isset($_SESSION['zalogowany']))
	{
	?>
		<div id="addPost" class="menuItem">
			<a href="Index.php?addPost=ok">Dodaj nowy post</a>
		</div>
		<div id="settings" class="menuItem">
			<a href="Index.php?settings=ok">Ustawienia</a>
		</div>
	<?php 
	}
	?>
	
	<div id="aboutMe" class="aboutMe">
		<?php include ('Menu/AboutMe.php'); ?>	
	</div>
	<div id="aboutCat" class="aboutCat">
		<?php include ('Menu/AboutCat.php'); ?>	
	</div>
	<div id="allPosts" class="allPosts">
		<?php include ('Menu/ArchiveOfPosts.php'); ?>	
	</div>			
</div>